const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const helmet = require('helmet')
const ical = require('node-ical')
const date = require('date-fns')

const initCitizenshipDate = new Date('2020-09-20')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())
app.use(helmet())

app.get('/date', async (req, res) => {
    await ical.async.fromURL('https://calendar.google.com/calendar/ical/4teord4qbm85c93pt3nobhge3g%40group.calendar.google.com/public/basic.ics')
        .then((daysOutsideCanada) => {        
            let daysToAdd = 0
            for (const event of Object.values(daysOutsideCanada)) {
                if (date.format(event.start, 'yyyy-MM-dd') === date.format(event.end, 'yyyy-MM-dd')) {
                    daysToAdd = daysToAdd + 1
                    continue
                }
                daysToAdd = daysToAdd + date.differenceInDays(event.start, event.end)
            }
            const newCitizenshipDate = date.format(date.addDays(initCitizenshipDate, daysToAdd), 'LLLL do yyyy')
            res.send({date: newCitizenshipDate})
        })
})

app.listen(process.env.PORT || 3001)